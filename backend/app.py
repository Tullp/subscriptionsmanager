
from datetime import datetime, timedelta
from uuid import uuid4
from fastapi import FastAPI, Body, Response
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
from pymongo import MongoClient, DESCENDING
from bson.objectid import ObjectId

app = FastAPI()
db = MongoClient("mongodb://mongo:27017").get_database("main")
users = db.get_collection("users")
tokens = db.get_collection("tokens")
subscriptions = db.get_collection("subscriptions")

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

def check_token(token):
	doc = tokens.find_one({"token": token}, sort=[("expiration", -1)])
	if doc and doc["expiration"] > datetime.now():
		return doc["user_id"]

def get_top_order(user_id: ObjectId):
	return (subscriptions.find_one(
		{"user_id": user_id},
		{"_id": 0, "order": 1},
		sort=[("order", DESCENDING)]
	) or {}).get("order", -1)

class Credentials(BaseModel):
	username: str
	password: str

class NewSubscription(BaseModel):
	title: str
	cost: float
	day: int
	color: str

class Subscription(BaseModel):
	id: str
	title: str
	cost: float
	day: int
	color: str
	order: int

@app.post("/register")
def register(credentials: Credentials, response: Response):
	if users.find_one(credentials.dict()):
		response.status_code = 400
		return {"ok": False, "reason": "Username was already taken"}
	users.insert_one(credentials.dict())
	user = users.find_one(credentials.dict())
	token = str(uuid4())
	tokens.insert_one({
		"user_id": user["_id"],
		"expiration": datetime.now() + timedelta(days=7),
		"token": token,
	})
	return {"ok": True, "token": token}

@app.post("/login")
def login(credentials: Credentials, response: Response):
	if not (user := users.find_one(credentials.dict())):
		response.status_code = 400
		return {"ok": False, "reason": "Username or password is incorrect"}
	token = str(uuid4())
	tokens.insert_one({
		"user_id": user["_id"],
		"expiration": datetime.now() + timedelta(days=7),
		"token": token,
	})
	return {"ok": True, "token": token}

@app.post("/getuser")
def get_user(response: Response, token: str = Body(embed=True)):
	if not (user_id := check_token(token)):
		response.status_code = 401
		return {"ok": False, "reason": "Not Authorized"}
	user = users.find_one({"_id": user_id})
	return {"ok": True, "username": user["username"]}

@app.post("/new")
def create_subscription(response: Response, subscription: NewSubscription, token: str = Body()):
	if not (user_id := check_token(token)):
		response.status_code = 401
		return {"ok": False, "reason": "Not Authorized"}
	sub_id = subscriptions.insert_one({
		**subscription.dict(),
		"user_id": user_id,
		"order": get_top_order(user_id) + 1,
		"ts": datetime.now(),
	}).inserted_id
	return {"ok": True, "subscription_id": str(sub_id)}

@app.post("/get")
def get_subscriptions(response: Response, token: str = Body(embed=True)):
	if not (user_id := check_token(token)):
		response.status_code = 401
		return {"ok": False, "reason": "Not Authorized"}
	subscriptions_list = [
		{**sub, "_id": str(sub["_id"])} for sub in
		subscriptions.find({"user_id": user_id}, {"user_id": 0, "ts": 0}).sort("order", DESCENDING)
	]
	return {"ok": True, "subscriptions": subscriptions_list}

@app.post("/delete")
def delete_subscription(response: Response, token: str = Body(), subscription_id: str = Body()):
	if not (user_id := check_token(token)):
		response.status_code = 401
		return {"ok": False, "reason": "Not Authorized"}
	if not subscriptions.find_one({"_id": ObjectId(subscription_id), "user_id": user_id}):
		response.status_code = 400
		return {"ok": False, "reason": "Invalid subscription id"}
	subscriptions.delete_one({"_id": ObjectId(subscription_id)})
	return {"ok": True}

@app.post("/edit")
def edit_subscription(response: Response, subscription: Subscription, token: str = Body()):
	if not (user_id := check_token(token)):
		response.status_code = 401
		return {"ok": False, "reason": "Not Authorized"}
	if not subscriptions.find_one({"_id": ObjectId(subscription.id), "user_id": user_id}):
		response.status_code = 400
		return {"ok": False, "reason": "Invalid subscription id"}
	subscriptions.update_one(
		{"_id": ObjectId(subscription.id)},
		{"$set": subscription.dict(exclude={"id"})}
	)
	return {"ok": True}

@app.post("/move")
def move_subscriptions(response: Response, token: str = Body(), from_id: str = Body(), to_id: str = Body()):
	if not (user_id := check_token(token)):
		response.status_code = 401
		return {"ok": False, "reason": "Not Authorized"}
	from_doc = subscriptions.find_one({"_id": ObjectId(from_id)})
	to_doc = subscriptions.find_one({"_id": ObjectId(to_id)})
	top_order = get_top_order(user_id)
	if from_doc["order"] > top_order or to_doc["order"] > top_order or from_doc["order"] < 0 or to_doc["order"] < 0:
		response.status_code = 400
		return {"ok": False, "reason": "Invalid data"}
	if from_doc["order"] > to_doc["order"]:
		subscriptions.update_many(
			{"user_id": user_id, "order": {
				"$gte": to_doc["order"], "$lt": from_doc["order"]
			}}, {"$inc": {"order": 1}}
		)
		subscriptions.update_one({"_id": from_doc["_id"]}, {"$set": {"order": to_doc["order"]}})
	elif from_doc["order"] < to_doc["order"]:
		subscriptions.update_many(
			{"user_id": user_id, "order": {
				"$gt": from_doc["order"], "$lte": to_doc["order"]
			}}, {"$inc": {"order": -1}}
		)
		subscriptions.update_one({"_id": from_doc["_id"]}, {"$set": {"order": to_doc["order"]}})
	return {"ok": True}

@app.post("/set_salary")
def set_salary(response: Response, token: str = Body(), salary: int = Body()):
	if not (user_id := check_token(token)):
		response.status_code = 401
		return {"ok": False, "reason": "Not Authorized"}
	users.update_one({"_id": user_id}, {"$set": {"salary": salary}})
	return {"ok": True}

@app.post("/get_salary")
def get_salary(response: Response, token: str = Body(embed=True)):
	if not (user_id := check_token(token)):
		response.status_code = 401
		return {"ok": False, "reason": "Not Authorized"}
	return {"salary": 0, **users.find_one({"_id": user_id}, {"salary": 1, "_id": 0})}
