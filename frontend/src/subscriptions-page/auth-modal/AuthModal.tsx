import styles from './auth-modal.module.scss';
import React, {useContext, useEffect, useState} from "react";
import profileIcon from "../../img/profile.svg";
import {Input} from "../../input/Input";
import {BackendContext} from "../SubscriptionsPage";
import {backendRequest} from "../../tools/utils";

interface i_props {
    onSuccess: (username: string, token: string) => void;
}

export const AuthModal = ({onSuccess}: i_props) => {
    const {on_401} = useContext(BackendContext);
    const [isLogin, setIsLogin] = useState<boolean>(true);
    const [username, setUsername] = useState<string>();
    const [password, setPassword] = useState<string>();
    const [confirmPassword, setConfirmPassword] = useState<string>();
    const [error, setError] = useState<string>();
    useEffect(() => {
        setError('');
    }, [isLogin]);
    const submit = () => {
        if (!username || !password)
            return setError('You should fill all fields.');
        if (!isLogin && password !== confirmPassword)
            return setError('Passwords don\'t match.');
        backendRequest({method: isLogin ? 'login' : 'register', on_401, body: {username, password}})
            .then(({ok, reason, token}) => ok ? onSuccess(username, token) : setError(reason))
            .catch((e: Error) => setError(e.message));
    }
    return <div className={styles.container}>
        <div className={styles.modal}>
            <div className={styles.form}>
                <div className={styles.title}>
                    <img src={profileIcon} alt=""/>
                    <p>{isLogin ? 'Log in' : 'Registration'}</p>
                </div>
                <div className={styles.row}>
                    <p>Username</p>
                    <Input onInput={setUsername}/>
                </div>
                <div className={styles.row}>
                    <p>Password</p>
                    <Input type="password" onInput={setPassword}/>
                </div>
                {!isLogin && <div className={styles.row}>
                    <p>Confirm Password</p>
                    <Input type="password" onInput={setConfirmPassword}
                        validate={async (value) => value === password}/>
                </div>}
                <button onClick={submit}>{isLogin ? 'Log in' : 'Register'}</button>
                {error && <p className={styles.error}>{error}</p>}
                {isLogin ? <p className={styles.switch_auth_method}>
                    Don't have an account? <span onClick={()=>setIsLogin(false)}>Register!</span>
                </p> : <p className={styles.switch_auth_method}>
                    Already have an account? <span onClick={()=>setIsLogin(true)}>Log in!</span>
                </p>}
            </div>
        </div>
    </div>;
};
