import React, {createContext, useEffect, useState} from 'react';
import styles from './subscriptions-page.module.scss';
import {SubscriptionsList} from './subscriptions-list/SubscriptionsList';
import {InfoPanel} from "./info-panel/InfoPanel";
import {colors} from "./letter-icon/LetterIcon";
import {backendRequest} from "../tools/utils";
import {AuthModal} from "./auth-modal/AuthModal";

export interface i_subscription {
    _id: string;
    title: string;
    cost: number;
    day: number;
    color: typeof colors[number];
    order: number;
}

interface i_userContext {
    username: string;
    subscriptions: i_subscription[];
    setSubscriptions: (subs: i_subscription[] | ((subs: i_subscription[]) => i_subscription[])) => void;
    updateSubscriptions: (cb?: () => void) => void;
    salary: number;
    setSalary: (salary: number) => void;
}

interface i_backendContext {
    token: string;
    on_401: () => void;
}

export const UserContext = createContext<i_userContext>({} as i_userContext);
export const BackendContext = createContext<i_backendContext>({} as i_backendContext);

export const SubscriptionsPage = () => {
    const [username, setUsername] = useState<string>('');
    const [token, setToken] = useState<string>(localStorage.getItem('token') || '');
    const [showAuthModal, setShowAuthModal] = useState<boolean>(!token);
    const [subscriptions, setSubscriptions] = useState<i_subscription[]>([]);
    const [salary, setSalary] = useState<number>(0);
    const on_401 = () => setShowAuthModal(true);
    const updateSubscriptions: (cb?: () => void) => void = (cb) => {
        backendRequest({method: 'get', on_401, token})
            .then((data) => {
                data.ok && setSubscriptions(data.subscriptions);
                cb && cb();
            });
    }
    useEffect(() => {
        backendRequest({method: 'getuser', on_401, token})
            .then((data) => data.ok && setUsername(data.username));
    }, [token]);
    useEffect(() => {
        updateSubscriptions();
    }, [token]);
    useEffect(() => {
        backendRequest({method: 'get_salary', on_401, token})
            .then((data) => setSalary(data.salary));
    }, [token]);
    return <BackendContext.Provider value={{on_401, token}}>
        <UserContext.Provider value={{username, subscriptions, setSubscriptions, updateSubscriptions, salary, setSalary}}>
            <div className={styles.container}>
                <SubscriptionsList className={styles.subscription_list}/>
                <InfoPanel className={styles.info_panel}/>
                {showAuthModal && <AuthModal onSuccess={(username, token) => {
                    localStorage.setItem('token', token);
                    setToken(token);
                    setShowAuthModal(false);
                }}/>}
            </div>
        </UserContext.Provider>
    </BackendContext.Provider>;
}
