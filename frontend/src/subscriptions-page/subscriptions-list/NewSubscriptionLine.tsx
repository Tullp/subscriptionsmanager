import React, {useContext, useEffect, useState} from 'react';
import styles from './subscriptions.module.scss';
import {backendRequest, cn} from '../../tools/utils';
import paletteIcon from '../../img/palette-icon.png';
import confirmIcon from '../../img/confirm-icon.svg';
import declineIcon from '../../img/decline-icon.svg';
import {Input} from '../../input/Input';
import {BackendContext, i_subscription, UserContext} from "../SubscriptionsPage";
import {LetterIcon, colors} from "../letter-icon/LetterIcon";

export const validateTitle = async (value: string) => value.length > 0;
export const validateCost = async (value: string) => /^\d+(\.\d+)?$/.test(value);
export const validateDay = async (value: string) => {
    const day = parseInt(value);
    return /^\d+$/.test(value) && 0 < day && day < 29;
};

export const NewSubscriptionLine = ({hide}: { hide: () => void }) => {
    const {on_401, token} = useContext(BackendContext);
    const {updateSubscriptions} = useContext(UserContext);
    const [showColorSelector, setShowColorSelector] = useState<boolean>(false);
    const [subscription, setSubscription] = useState<any>({});
    const [colorError, setColorError] = useState<boolean>(false);
    const setField = (field: keyof i_subscription) => {
        return (value: string) => setSubscription((sub: any) => ({...sub, [field]: value}));
    };
    const submit = async () => {
        if (!await validateTitle(subscription.title))
            return;
        if (!await validateCost(subscription.cost))
            return;
        if (!await validateDay(subscription.day))
            return;
        if (!colors.includes(subscription.color)) {
            let count = 0;
            const switching: any = () => {
                if (!colors.includes(subscription.color) && count < 10) {
                    setColorError((val) => !val);
                    count += 1;
                    return setTimeout(switching, 250);
                }
            };
            return switching();
        }
        const body = {subscription: {
            title: subscription.title,
            cost: parseFloat(subscription.cost),
            day: parseInt(subscription.day),
            color: subscription.color,
        }};
        const data = await backendRequest({method: 'new', on_401, token, body});
        if (!data.ok)
            return;
        updateSubscriptions(hide)
    };
    useEffect(() => {
        if (!showColorSelector)
            return;
        let firstClick = false;
        const listener = () => {
            if (!firstClick)
                return firstClick = true;
            setShowColorSelector(false);
        };
        document.addEventListener('click', listener);
        return () => document.removeEventListener('click', listener);
    }, [showColorSelector]);
    return <div className={styles.subscription_line} style={{order: -99999}}>
        <div className={cn(styles.cell, styles.icon_cell)}>
            {!subscription.color && <div onClick={() => setShowColorSelector(!showColorSelector)}
                    className={cn(styles.color_selector_icon, colorError && styles.color_selector_icon_error)}>
                <img src={paletteIcon} width={30} alt=""/>
            </div>}
            {subscription.color && <LetterIcon color={subscription.color}
                letter={subscription.title?.length ? subscription.title[0].toUpperCase() : 'A'}
                onClick={() => setShowColorSelector(!showColorSelector)}/>}
            {showColorSelector && <div className={styles.color_selector}>
                {colors.map((schema) => <LetterIcon color={schema} key={schema}
                    onClick={() => setField('color')(schema)}
                    letter={subscription.title?.length ? subscription.title[0].toUpperCase() : 'A'}/>)}
            </div>}
        </div>
        <div className={cn(styles.cell, styles.title_cell)}>
            <Input width={160} validate={validateTitle} onInput={setField('title')}/>
        </div>
        <div className={cn(styles.cell, styles.cost_cell)}>
            <Input width={80} textAlign="center" validate={validateCost} onInput={setField('cost')}/>
        </div>
        <div className={cn(styles.cell, styles.day_cell)}>
            <Input width={60} textAlign="right" validate={validateDay} onInput={setField('day')}/>
        </div>
        <div className={cn(styles.cell, styles.controls_cell)}>
            <div className={styles.control} onClick={submit}>
                <img src={confirmIcon} width={16} alt=""/>
            </div>
            <div className={styles.control} onClick={hide}>
                <img src={declineIcon} width={16} alt=""/>
            </div>
        </div>
    </div>;
};
