import React, {useContext, useState} from 'react';
import styles from './subscriptions.module.scss';
import plusIcon from '../../img/plus-icon.svg';
import {NewSubscriptionLine} from "./NewSubscriptionLine";
import {SubscriptionLine} from "./SubscriptionLine";
import {BackendContext, i_subscription, UserContext} from "../SubscriptionsPage";
import {backendRequest} from "../../tools/utils";

export const SubscriptionsList = ({className}: {className: string}) => {
    const {on_401, token} = useContext(BackendContext)
    const {subscriptions, updateSubscriptions} = useContext(UserContext);
    const [show_new_sub_line, setShowNewSubLine] = useState<boolean>(false);
    const [dragged, setDragged] = useState<i_subscription>();
    const changeOrder = (dragged: i_subscription, dropped: i_subscription) => {
        if (dragged._id === dropped._id) return;
        backendRequest({method: 'move', on_401, token, body: {from_id: dragged._id, to_id: dropped._id}})
            .then((data) => data.ok && updateSubscriptions());
    }
    return <div className={className}>
        <div className={styles.header}>
            <p className={styles.title}>Subscriptions:</p>
            <button className={styles.new_subscription_button}
                    onClick={() => setShowNewSubLine(true)}>
                <img src={plusIcon} width={18} alt=""/> Add
            </button>
        </div>
        <div className={styles.table_header}>
            <p className={styles.title_header}>Service title</p>
            <p className={styles.cost_header}>Price</p>
            <p className={styles.day_header}>Day of month</p>
        </div>
        <div className={styles.subscriptions_container} onDragOver={(e) => e.preventDefault()}>
            {show_new_sub_line && <NewSubscriptionLine
                hide={() => setShowNewSubLine(false)}/>}
            {subscriptions?.length > 0 && subscriptions
                .map((sub) => <SubscriptionLine key={sub._id} subscription={sub}
                    startDrag={() => setDragged(sub)} onDrop={() => dragged && changeOrder(dragged, sub)}/>)}
        </div>
    </div>;
};