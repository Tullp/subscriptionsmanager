import React, {Fragment, useContext, useEffect, useState} from "react";
import styles from "./subscriptions.module.scss";
import {BackendContext, i_subscription, UserContext} from "../SubscriptionsPage";
import {backendRequest, cn} from "../../tools/utils";
import {LetterIcon, colors} from "../letter-icon/LetterIcon";
import editIcon from "../../img/edit-icon.svg";
import trashIcon from "../../img/trash-icon.svg";
import {Input} from "../../input/Input";
import confirmIcon from "../../img/confirm-icon.svg";
import declineIcon from "../../img/decline-icon.svg";
import {validateCost, validateDay, validateTitle} from "./NewSubscriptionLine";

interface i_props {
    subscription: i_subscription;
    startDrag: () => void;
    onDrop: () => void;
}

export const SubscriptionLine = ({subscription, startDrag, onDrop}: i_props) => {
    const {on_401, token} = useContext(BackendContext);
    const {updateSubscriptions} = useContext(UserContext);
    const [editedSubscription, setEditedSubscription] = useState(subscription as any);
    const [editMode, setEditMode] = useState<boolean>(false);
    const [showColorSelector, setShowColorSelector] = useState<boolean>(false);
    const setField = (field: keyof i_subscription) => {
        return (value: string) => setEditedSubscription((sub: any) => ({...sub, [field]: value}));
    };
    const editSubscription = async () => {
        if (!await validateTitle(editedSubscription.title))
            return;
        if (!await validateCost(editedSubscription.cost))
            return;
        if (!await validateDay(editedSubscription.day))
            return;
        if (!colors.includes(editedSubscription.color))
            return;
        const body = {subscription: {
            id: subscription._id,
            title: editedSubscription.title,
            cost: parseFloat(editedSubscription.cost),
            day: parseInt(editedSubscription.day),
            color: editedSubscription.color,
            order: subscription.order,
        }};
        const data = await backendRequest({method: 'edit', on_401, token, body});
        if (!data.ok)
            return;
        updateSubscriptions(() => setEditMode(false));
    };
    const deleteSubscription = async () => {
        const data = await backendRequest({method: 'delete', on_401, token,
            body: {subscription_id: subscription._id}});
        if (!data.ok)
            return;
        updateSubscriptions();
    };
    useEffect(() => {
        let firstClick = false;
        const listener = () => {
            if (!firstClick)
                return firstClick = true;
            if (showColorSelector)
                setShowColorSelector(false);
        };
        document.addEventListener('click', listener);
        return () => document.removeEventListener('click', listener);
    }, [showColorSelector]);
    return <div className={styles.subscription_line} style={{order: -subscription.order}}
            draggable={true} onDragStart={startDrag} onDrop={(e) => {e.preventDefault(); onDrop()}}>
        <div className={cn(styles.cell, styles.icon_cell)}>
            {!editMode && <LetterIcon letter={subscription.title?.length ? subscription.title[0].toUpperCase() : 'A'}
                color={subscription.color}/>}
            {editMode && <LetterIcon letter={editedSubscription.title[0]?.toUpperCase() || 'A'}
                color={subscription.color} onClick={() => setShowColorSelector(!showColorSelector)}/>}
            {editMode && showColorSelector && <div className={styles.color_selector}>
                {colors.map((schema) => <LetterIcon color={schema} key={schema}
                    letter={editedSubscription.title[0]?.toUpperCase() || 'A'}
                    onClick={() => setEditedSubscription((sub: any) => ({...sub, color: schema}))}/>)}
            </div>}
        </div>
        <div className={cn(styles.cell, styles.title_cell)}>
            {!editMode && <p>{subscription.title}</p>}
            {editMode && <Input width={160} validate={validateTitle}
                onInput={setField('title')} value={editedSubscription.title}/>}
        </div>
        <div className={cn(styles.cell, styles.cost_cell)}>
            {!editMode && <p>${subscription.cost}</p>}
            {editMode && <Input width={80} validate={validateCost} textAlign="center"
                onInput={setField('cost')} value={editedSubscription.cost}/>}
        </div>
        <div className={cn(styles.cell, styles.day_cell)}>
            {!editMode && <p>{subscription.day}th</p>}
            {editMode && <Input width={60} validate={validateDay} textAlign="right"
                onInput={setField('day')} value={editedSubscription.day}/>}
        </div>
        <div className={cn(styles.cell, styles.controls_cell)}>
            {!editMode && <Fragment>
                <div className={styles.control} onClick={() => setEditMode(true)}>
                    <img src={editIcon} width={16} alt=""/>
                </div>
                <div className={styles.control} onClick={deleteSubscription}>
                    <img src={trashIcon} width={16} alt=""/>
                </div>
            </Fragment>}
            {editMode && <Fragment>
                <div className={styles.control} onClick={editSubscription}>
                    <img src={confirmIcon} width={16} alt=""/>
                </div>
                <div className={styles.control} onClick={() => setEditMode(false)}>
                    <img src={declineIcon} width={16} alt=""/>
                </div>
            </Fragment>}
        </div>
    </div>;
}