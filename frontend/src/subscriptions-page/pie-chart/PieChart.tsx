
import React from 'react';
import styles from './pie-chart.module.scss';
import {cn} from "../../tools/utils";

export type colors = 'orange' | 'green' | 'blue';

interface i_props {
    color: colors;
    percentage: number | string;
    width: number;
}

export const PieChart = ({color, percentage, width}: i_props) => {
    return <div className={styles.chart} style={{width: `${width}px`}}>
        <svg viewBox="0 0 36 36" className={cn(styles.circular_chart, styles[color])}>
            <path className={styles.circle_bg}
                d="M18 2.0845
                   a 15.9155 15.9155 0 0 1 0 31.831
                   a 15.9155 15.9155 0 0 1 0 -31.831"/>
            <path className={styles.circle} strokeDasharray={`${percentage}, 100`}
                d="M18 2.0845
                   a 15.9155 15.9155 0 0 1 0 31.831
                   a 15.9155 15.9155 0 0 1 0 -31.831"/>
            <text x="19" y="18" className={styles.percentage}>{percentage}%</text>
            <text x="18" y="23" className={styles.percentage_description}>of income</text>
        </svg>
    </div>
}
