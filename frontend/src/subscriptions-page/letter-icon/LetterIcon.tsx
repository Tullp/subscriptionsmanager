
import React from "react";
import styles from './letter-icon.module.scss';
import {cn} from "../../tools/utils";

export const colors = ['red', 'yellow', 'green', 'pink', 'purple', 'blue'] as const;

interface i_props {
    letter: string;
    color: typeof colors[number];
    onClick?: () => void;
}

export const LetterIcon = ({letter, color, onClick}: i_props) => {
    return <div className={cn(styles.container, styles[color], onClick && styles.pointer)} onClick={onClick}>
        <p>{letter}</p>
    </div>
}
