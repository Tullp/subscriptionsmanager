import React, {Fragment, useContext, useState, FocusEvent} from "react";
import styles from './info-panel.module.scss';
import profileIcon from '../../img/profile.svg';
import settingsIcon from '../../img/settings-icon.svg';
import editIcon from '../../img/edit-icon.svg';
import {BackendContext, UserContext} from "../SubscriptionsPage";
import {backendRequest} from "../../tools/utils";
import {Input} from "../../input/Input";
import {PieChart} from "../pie-chart/PieChart";

export const InfoPanel = ({className}: { className: string }) => {
    const {on_401, token} = useContext(BackendContext);
    const {username, salary, setSalary, subscriptions} = useContext(UserContext);
    const [salaryEditMode, setSalaryEditMode] = useState<boolean>(false);
    const updateSalary = (e: FocusEvent<HTMLInputElement>) => {
        const salary = parseInt(e.target.value);
        if (e.target.value && !Number.isNaN(salary))
            backendRequest({method: 'set_salary', on_401, token, body: {salary}})
                .then(data => {
                    if (data.ok) setSalary(salary);
                    setSalaryEditMode(false);
                });
        else setSalaryEditMode(false);
    };
    const subscriptions_sum = subscriptions.reduce((sum, sub) => sum + sub.cost, 0);
    return <div className={className}>
        <div className={styles.header}>
            <img src={profileIcon} width={46} alt=""/>
            <p className={styles.username}>{username}</p>
            <div className={styles.settings_icon}>
                <img src={settingsIcon} width={28} alt=""/>
            </div>
        </div>
        <div className={styles.profit_container}>
            <p>Your profit</p>
            <div className={styles.profit_amount_container}>
                {!salaryEditMode && <Fragment>
                    <p>{salary}</p>
                    <div className={styles.profit_edit_icon}
                         onClick={() => setSalaryEditMode(true)}>
                        <img src={editIcon} width={14} alt=""/>
                    </div>
                </Fragment>}
                {salaryEditMode && <Input className={styles.salary_edit}
                    onBlur={updateSalary} textAlign="center" autoFocus/>}
            </div>
            <p>USD/month</p>
        </div>
        <div className={styles.subscriptions_cost_container}>
            <p>Subscription</p>
            <p className={styles.subscriptions_cost}>
                {subscriptions_sum.toFixed(2)}
            </p>
            <p>USD/month</p>
        </div>
        <div className={styles.pie_chart}>
            <p>You spent on subscriptions:</p>
            <PieChart color={'orange'} percentage={(subscriptions_sum / salary * 100).toFixed(0)} width={160}/>
        </div>
    </div>;
};
