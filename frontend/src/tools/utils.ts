
export const cn = (...classes: (string | undefined | false)[]) => {
    return classes.filter(c => !!c).join(' ');
};

export const backend = () => {
    return `${window.location.protocol}//${window.location.hostname}:8000`;
};

interface i_backendRequest {
    method: string;
    body?: any;
    on_401?: () => void;
    token?: string;
}

export const backendRequest = ({method, body, on_401, token}: i_backendRequest) => {
    body = body || {};
    if (token)
        body.token = token;
    return fetch(`${window.location.protocol}//${window.location.hostname}:8000/${method}`, {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(body),
    }).then((response) => {
        if (response.status === 401) on_401 && on_401();
        else return response.json();
    });
};
