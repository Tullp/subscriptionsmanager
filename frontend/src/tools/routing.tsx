import {useState} from 'react';
import {SubscriptionsPage} from '../subscriptions-page/SubscriptionsPage';
import {LoginPage} from '../login-page/LoginPage';
import {RegistrationPage} from '../registration-page/RegistrationPage';

export interface i_routing {
    route: string;
    setRoute: (route: string) => void;
    getComponent: (route: string) => JSX.Element;
}

export const useRouting: () => i_routing = () => {
    const [route, setRoute] = useState<string>('');
    return {route, setRoute, getComponent};
}

const getComponent = (route: string) => {
    switch (route || 'subscriptions') {
        case 'subscriptions': return <SubscriptionsPage/>;
        case 'login': return <LoginPage/>;
        case 'registration': return <RegistrationPage/>;
        default: return <div/>;
    }
}
