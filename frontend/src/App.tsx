import React, {createContext} from 'react';
import {i_routing, useRouting} from './tools/routing';

export const RoutingContext = createContext<i_routing>({} as i_routing);

export const App = () => {
  const routing = useRouting();
  return <RoutingContext.Provider value={routing}>
    {routing.getComponent(routing.route)}
  </RoutingContext.Provider>
}
