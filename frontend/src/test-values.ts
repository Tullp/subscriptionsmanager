import {i_subscription} from './subscriptions-page/SubscriptionsPage';

export const test_subscriptions: i_subscription[] = [
  {_id: '', title: 'Netflix', cost: 14.99, day: 29, color: 'red', order: 3},
  {_id: '', title: 'Youtube', cost: 15, day: 1, color: 'red', order: 2},
  {_id: '', title: 'Figma', cost: 12, day: 1, color: 'purple', order: 1},
  {_id: '', title: 'Patreon', cost: 5, day: 15, color: 'yellow', order: 0},
];

export const test_salary = 6250;
