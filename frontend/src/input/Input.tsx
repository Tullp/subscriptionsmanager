
import React from 'react';
import styles from './input.module.scss';
import {cn} from "../tools/utils";
import {useEffect, useState} from "react";

interface i_props {
    className?: string;
    width?: number;
    textAlign?: 'left' | 'center' | 'right';
    w100?: boolean;
    validate?: (value: string) => Promise<boolean>;
    onInput?: (value: string) => void;
    [key: string]: any;
}

export const Input = (props: i_props) => {
    const {className, width, textAlign = 'left', w100 = true,
        validate = async () => true, onInput = () => {}, ...undefinedProps} = props;
    const [value, setValue] = useState<string>();
    const [error, setError] = useState<boolean>(false);
    const onInputHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
        setValue(e.target.value);
        onInput(e.target.value);
    }
    useEffect(() => {
        value !== undefined && validate(value).then(ok => setError(!ok));
    }, [value, validate]);
    return <input type="text" className={cn(styles.input, w100 && styles.w100, error && styles.error, className)}
        style={{width: `${width}px`, textAlign}} onInput={onInputHandler} {...undefinedProps}/>
};
